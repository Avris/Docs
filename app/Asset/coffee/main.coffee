
$('body')
  .scrollspy
    target: 'nav'
  .on 'activate.bs.scrollspy', (e) ->
    hash = $('nav li.active:last a').attr('href')
      #e.target.children[0].hash
    if window.history.pushState
      window.history.pushState(null, null, hash);
    else
      window.location.hash = hash

nav = $('nav>.nav')
footer = $('footer')

resize = ->
  nav.css('height', window.innerHeight - 23)
resize()
$(window).resize(resize)

$('#hamburger').click ->
  nav.toggleClass('shown')
  footer.toggleClass('shown')