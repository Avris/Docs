<?php
namespace App\Controller;

use App\Model\Project;
use App\Service\DocsFetcher;
use Avris\Micrus\Controller\Controller;
use Avris\Micrus\Exception\UnauthorisedException;

class DocsController extends Controller
{
    public function homeAction()
    {
        $projects = $this->getService('orm')->findAll('project');

        if (count($projects) == 1) {
            return $this->redirectToRoute('docsShow', ['project' => reset($projects)->getId()]);
        }

        return $this->render([
            'projects' => $projects,
        ]);
    }

    public function showAction(Project $project, $branch = null)
    {
        /** @var DocsFetcher $fetcher */
        $fetcher = $this->getService('avris_docsFetcher')->init($project);

        $tags = $fetcher->getTags();

        if (!$branch) {
            $branch = isset($tags[0]) ? $tags[0] : 'master';
        }

        array_unshift($tags, 'master');

        if (!in_array($branch, $tags)) {
            throw new UnauthorisedException;
        }

        return $this->render([
            'project' => $project,
            'docs' => $fetcher->fetchDocs($branch),
            'tags' => $tags,
            'currentTag' => $branch,
        ]);
    }

    public function updateAction(Project $project)
    {
        $this->getService('avris_docsFetcher')
            ->init($project)
            ->update();

        return $this->redirectToRoute('docsShow', ['project' => $project->getId()]);
    }
}
