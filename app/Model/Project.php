<?php
namespace App\Model;

class Project
{
    /** @var string */
    protected $id;

    /** @var string */
    protected $name;

    /** @var string */
    protected $description;

    /** @var string */
    protected $repository;

    /** @var string */
    protected $website;

    /**
     * @param string $id
     * @param string $name
     * @param string $description
     * @param string $repository
     * @param string $website
     */
    public function __construct($id, $name, $description, $repository, $website)
    {
        $this->id = $id;
        $this->name = $name;
        $this->description = $description;
        $this->repository = $repository;
        $this->website = $website;
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     * @return Project
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @param bool $full
     * @return string
     */
    public function getRepository($full = true)
    {
        return $this->repository . ($full ? '.git' : '');
    }

    /**
     * @param string $repository
     * @return Project
     */
    public function setRepository($repository)
    {
        $this->repository = $repository;
        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Project
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return Project
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return string
     */
    public function getWebsite()
    {
        return $this->website;
    }

    /**
     * @param string $website
     * @return Project
     */
    public function setWebsite($website)
    {
        $this->website = $website;
        return $this;
    }

}
