<?php
namespace App\Model;

use Avris\Micrus\Model\ORM;

class ProjectProvider implements ORM
{
    protected $projects;

    public function __construct()
    {
        $this->projects = [
            'micrus' => new Project(
                'micrus',
                'Micrus',
                'Tiny, yet full-stack, PHP framework',
                'https://gitlab.com/Avris/Micrus',
                'https://micrus.avris.it'
            ),
            'picco' => new Project(
                'picco',
                'Picco',
                'Golfy PHP framework',
                'https://gitlab.com/Avris/Picco',
                'https://picco.avris.it'
            ),
            'qc' => new Project(
                'qc',
                'QC',
                'Quite Concise Programming Language',
                'https://gitlab.com/Avris/QC',
                'https://qc.avris.it'
            ),
            'stringer' => new Project(
                'stringer',
                'Stringer',
                'Zestaw przydatnych helperów językowych',
                'https://gitlab.com/Avris/Stringer',
                null
            ),
        ];
    }

    public function getEntityManager()
    {
        return null;
    }

    /**
     * @param string $type
     * @param string $id
     * @return Project
     */
    public function find($type, $id)
    {
        return isset($this->projects[$id]) ? $this->projects[$id] : null;
    }

    /**
     * @param string $type
     * @return Project[]
     */
    public function findAll($type)
    {
        return $this->projects;
    }

    public function findBy($type, $attribute, $value)
    {
        return [];
    }

    public function findOneBy($type, $attribute, $value)
    {
        return [];
    }
}
