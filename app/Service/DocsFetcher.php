<?php
namespace App\Service;

use App\Model\Project;
use Avris\Micrus\Exception\InvalidArgumentException;
use Avris\Micrus\Tool\Cache\Cacher;
use GitWrapper\GitException;
use GitWrapper\GitWorkingCopy;
use GitWrapper\GitWrapper;

class DocsFetcher
{
    /** @var Cacher */
    protected $cacher;

    /** @var DocsFormatter */
    protected $formatter;

    /** @var Project */
    protected $project;

    /** @var string */
    protected $baseDir;

    /** @var string */
    protected $dir;

    /** @var GitWorkingCopy */
    protected $git;

    public function __construct(Cacher $cacher, DocsFormatter $docsFormatter, $baseDir)
    {
        putenv("DYLD_LIBRARY_PATH=''");

        $this->cacher = $cacher;
        $this->formatter = $docsFormatter;
        $this->baseDir = $baseDir;
    }

    public function init(Project $project)
    {
        $this->project = $project;
        $this->dir = $this->baseDir . md5($project->getRepository()) . '/';

        $this->git = new GitWorkingCopy(new GitWrapper(), $this->dir);
        if (!file_exists($this->dir . '.git')) {
            $this->git->init();
            $this->git->setCloned(true);
            $this->git->remote('add', 'origin', $project->getRepository());
        }

        return $this;
    }

    public function update()
    {
        $this->assertInitialized();
        $this->fetchDocs('master', true);
        foreach ($this->getTags(true) as $tag) {
            $this->fetchDocs($tag);
        }
    }

    public function getTags($force = false)
    {
        $this->assertInitialized();
        return $this->cacher->cache('docs/' . md5($this->project->getRepository()) . '.tags.obj', function() {
            $this->git->clearOutput();
            $this->git->fetch('origin');
            $this->git->tag();
            $output = trim($this->git->getOutput());
            $tags = $output ? explode("\n", $output) : [];
            rsort($tags);
            return $tags;
        }, [], $force ? Cacher::MODE_ALWAYS_MISS : Cacher::MODE_NORMAL);
    }

    public function fetchDocs($branch = 'master', $force = false)
    {
        $this->assertInitialized();
        return $this->cacher->cache(
            'docs/' . md5($this->project->getRepository()) . '.' . $branch.'.obj',
            function($branch) {
                $this->git->fetch('origin');
                try {
                    $this->git->checkout($branch == 'master' ? 'origin/master' : 'tags/'.$branch, '--', 'docs');
                } catch (GitException $e) {}
                try {
                    $this->git->checkout($branch == 'master' ? 'origin/master' : 'tags/'.$branch, '--', 'README.md');
                    @mkdir($this->dir . 'docs/');
                    @symlink($this->dir . 'README.md', $this->dir . 'docs/000_readme.md');
                } catch (GitException $e) {}

                return $this->formatter->generateDocs($this->dir . 'docs/');
            },
            [$branch],
            $force ? Cacher::MODE_ALWAYS_MISS : Cacher::MODE_NORMAL
        );
    }

    protected function assertInitialized()
    {
        if (!$this->project) {
            throw new InvalidArgumentException('No project loaded');
        }
    }

}