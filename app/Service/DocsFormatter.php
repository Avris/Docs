<?php
namespace App\Service;

class DocsFormatter

{
    protected $chapters = [];

    protected $currentChapter;

    public function generateDocs($dir)
    {
        $md = new \Parsedown();
        $content = '';
        $this->chapters = [];

        $files = glob($dir . '*.md');
        asort($files);

        foreach ($files as $file) {
            $dom = new \DOMDocument();
            $dom->encoding = 'UTF-8';
            @$dom->loadHtml('<meta charset="UTF-8"/>'.$md->parse(file_get_contents($file)));
            $this->handleNode($dom, basename($file) == '000_readme.md');
            $this->addChapter();
            $content .= substr($dom->saveHTML($dom->getElementsByTagName('body')->item(0)), 6, -7);
        }

        return [
            'chapters' => $this->chapters,
            'content' => $content,
        ];
    }

    protected function handleNode(\DOMNode $node, $isReadme = false)
    {
        if ($node instanceof \DOMText) {
            if ($node->parentNode instanceof \DOMElement &&
                in_array($node->parentNode->tagName, ['pre','code'])
            ) {
                return;
            }
            if (!trim($node->wholeText)) {
                return;
            }
            $replaced = str_replace('--', '–', $node->wholeText, $count);
            if ($count) {
                $node->replaceData(0, strlen($node->wholeText), $replaced);
            }
        }

        if ($node instanceof \DOMElement) {
            if ($node->tagName == 'h2' || $node->tagName == 'h1') {
                if ($isReadme) {
                    $text = $node->childNodes->item(0);
                    if ($text instanceof \DOMText) {
                        $text->replaceData(0, strlen($text->wholeText), '');
                    }
                    $node->setAttribute('style', 'padding: 0;margin: 0;');
                    $node->setAttribute('id', 'readme');
                    $this->addChapter();
                    $this->currentChapter = [
                        'id' => 'readme',
                        'name' => 'Readme',
                        'subchapters' => [],
                    ];
                } else {
                    $id = $this->buildId($node->textContent);
                    $node->setAttribute('id', $id);
                    $this->addChapter();
                    $this->currentChapter = [
                        'id' => $id,
                        'name' => $node->textContent,
                        'subchapters' => [],
                    ];
                }
            } else if ($node->tagName == 'h3') {
                $id = $this->buildId($this->currentChapter['name'] . '-' . $node->textContent);
                $node->setAttribute('id', $id);
                $this->addSubchapter([
                    'id' => $id,
                    'name' => $node->textContent,
                ]);
            } else if ($node->tagName == 'table') {
                $node->setAttribute('class', 'table table-striped');
            }

        }

        if (count($node->childNodes)) {
            foreach ($node->childNodes as $childNode) {
                $this->handleNode($childNode, $isReadme);
            }
        }
    }

    protected function addChapter()
    {
        if ($this->currentChapter) {
            $this->chapters[] = $this->currentChapter;
            $this->currentChapter = null;
        }
    }

    protected function addSubchapter($data)
    {
        if ($this->currentChapter) {
            $this->currentChapter['subchapters'][] = $data;
        }
    }

    protected function buildId($name)
    {
        return str_replace([' ', '(', ')'], ['-', '', ''], strtolower($name));
    }
}